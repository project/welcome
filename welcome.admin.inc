<?php
/**
 * @file
 * Admin page callbacks for the welcome module.
 */

/**
 * Implements hook_form().
 * Admin form to configurable welcome message
 */
function welcome_admin_settings($form) {

  $token_help = t('Available variables are: [site:name], [site:url], [user:name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url].');

  $form['#tree'] = TRUE;

  // Get saved messages.
  $query = db_select('welcome_message', 'wm')
    ->fields('wm', array('rid', 'message', 'weight'))
    ->orderBy('weight');
  $result = $query->execute();

  foreach ($result as $record) {
    $role = user_role_load($record->rid);

    $form['welcome_message'][$record->rid]['msg'] = array(
        '#type' => 'textarea',
        '#title' => t('Welcome message for @role', array('@role' => $role->name)),
        '#description' => t('Edit the welcome message.') . ' ' . $token_help,
        '#rows' => 5,
        '#default_value' => $record->message,
    );

    $form['welcome_message'][$record->rid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 10,
        '#default_value' => $record->weight,
    );

    $form['welcome_message'][$record->rid]['current'] = array(
      '#type' => 'value',
      '#value' => $record->rid,
    );
  }

  // Add fields for any roles not already included
  $roles = user_roles(TRUE);
  foreach($roles as $rid => $role) {
    if(isset($form['welcome_message'])) {
      $form_keys = $form['welcome_message'];
    }
    else{
      $form_keys = array();
    }

    if (!array_key_exists($rid, $form_keys)) {
      $form['welcome_message'][$rid]['msg'] = array(
        '#type' => 'textarea',
        '#title' => t('Welcome message for @role', array('@role' => $role)),
        '#rows' => 5,
        '#default_value' => "",
      );

      $form['welcome_message'][$rid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 10,
        '#default_value' => 10,
      );
    }
  }


  // Submit button
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function welcome_admin_settings_submit($form, &$form_state) {
  foreach ($form_state['values']['welcome_message'] as $id => $data) {
    if (is_array($data) && isset($data['weight'])) {
      $table = 'welcome_message';
      $record = array('rid' => $id, 'message' => $data['msg'], 'weight' => $data['weight']);
      if (empty($data['current']) || !isset($data['current'])) {
        drupal_write_record($table, $record);
      }
      else {
        drupal_write_record($table, $record, array('rid'));
      }
    }
    drupal_set_message(t('Saved welcome messages'));
  }
}

/**
 * Returns HTML for the welcome admin form.
 */
function theme_welcome_admin_settings($variables) {
  $form = $variables['form'];
  $rows = array();
  foreach(element_children($form['welcome_message']) as $id) {
    $form['welcome_message'][$id]['weight']['#attributes']['class'] = array('text-welcome-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['welcome_message'][$id]['msg']),
        drupal_render($form['welcome_message'][$id]['weight']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(t('Message'), t('Weight'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'text-welcome-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('text-welcome-order', 'order', 'sibling', 'text-welcome-order-weight');

  return $output;
}
